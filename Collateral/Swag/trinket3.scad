// Designed by Ubuntourist <ubuntourist@hacdc.org> 2019.05.13
//
// Having failed with my original method, I humbly follow in
// in the footsteps of James Sullivan (who got a bit of an assist
// from Julia Longtin, as I understand).
//
// logo-168.png ....... is the graphic without the HAC DC text
// logo-split.png ..... contains the HAC DC text split on two lines
//
// Final dimensions (mm): ~ (22 x 45 x 1)
// Converted to G-Code with Cura 4.0.0 (Ultimaker edition)
// Printed on the Ultimaker 2 in ~30 minutes (from cold to cold)
//                                           (Cold = 29 C, 40 C)

scale([2.0/3.0, 2.0/3.0, 1])
  union() {
    translate([0, -0.2, 0.5])
      cube([34, 60, 1], center=true);
    difference() {
      translate([0, 20, 0])
        cylinder(r=17, h=1, $fn=200);
      translate([0, 32.3, -2])
        cylinder(r=2.5, h=4, $fn=200);
    }
    translate([0, 0, 1.5])
      scale([0.1, 0.1, 0.01])
        surface(file="logo-split.png", center=true, invert=true);
  }