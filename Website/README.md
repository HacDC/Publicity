# Website (WordPress) backup

Herein you will (eventually) find the assets that make the HacDC
**WordPress** site what it is, including, but not limited to:

* database Content table(s)
* database Settings table
* images directory
* files directory
* theme customizations (including CSS files)
