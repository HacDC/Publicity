#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Draw the Fibonacci Spiral using Turtle Graphics
# Copied by Ubuntourist <ubuntourist@hacdc.org> 2019.04.25 (kjc)
# from https://trinket.io/python/43bc79b582

import turtle

# Fibonacci numbers
#
FIBO_NR = [1,1,2,3, 5, 8, 13, 21, 34,55]  # These could be calculated instead


def draw_square(side_length):
    """Draws a square of length side_length"""
    for i in range(4):
        turtle.forward(side_length)
        turtle.right(90)

NR_SQUARES = len(FIBO_NR)

FACTOR = 3                               # Enlargement factor
turtle.penup()
turtle.goto(50,50)                       # Move starting point right and up
turtle.pendown()
for i in range(NR_SQUARES):
    draw_square(FACTOR * FIBO_NR[i])     # Draw square
    turtle.penup()                       # Move to new corner as starting point
    turtle.forward(FACTOR * FIBO_NR[i])
    turtle.right(90)
    turtle.forward(FACTOR * FIBO_NR[i])
    turtle.pendown()

turtle.penup()
turtle.goto(50,50)       # Move to starting point
turtle.setheading(0)     # Face the turtle to the right
turtle.pencolor('red')
turtle.pensize(3)
turtle.pendown()

# Draw quartercircles with Fibonacci numbers as radius
#
for i in range(NR_SQUARES):
    turtle.circle(-FACTOR * FIBO_NR[i],90)  # minus sign to draw clockwise
